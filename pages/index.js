import React from "react"

export default function Home({lat}) {
  const [tz, setTz] = React.useState('loading...')
  React.useEffect(() => {
    (async() => {
        const res = await fetch('/api/geo')
        if(res.ok){
          const json = await res.json();
          setTz(json.tz)
        }
    })()
  }, [])

  return (
    <div>Test: {tz}</div>
  )
}

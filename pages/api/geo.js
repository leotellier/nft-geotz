
import { find as findTimezone } from 'geo-tz'

export default function handler(_, res) {
  const tz = findTimezone(-21.2377437, 55.48997639438238)  // ['Indian/Reunion']
  res.status(200).json({ tz })
}
